import express from 'express';
import cors from 'cors'
import 'dotenv/config'
import Arbolrouter from './routes/arbol.js'
import Reservarouter from './routes/reservas.js'
import CoursesRouter from './routes/cursos.js';
import Authrouter from './routes/auth.js'
import { verifyToken } from "./middleware/auth.js"
import cookieParser from "cookie-parser"
import mongoose from 'mongoose'
import Semillarouter from './routes/semillas.js';
import Userrouter from './routes/users.js';
import bodyParser from 'body-parser'
import Paisrouter from './routes/paises.js';
import CheckoutRouter from './routes/checkout.js';
import UploadRouter from './routes/upload.js';
import path from 'path';
import {fileURLToPath} from 'url';
import nodemailer from 'nodemailer';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const app = express()

// Initialize Database along with models - this is how Mongoose does this.
// Most probably you will have them defined in a separate file
const mongoDB = 'mongodb://localhost:27017/test';
//const mongoDB = 'mongodb+srv://jbgranja:mongo1506@cluster0.ndjj8c5.mongodb.net/?retryWrites=true&w=majority';
app.use('/public', express.static('public'))
mongoose.set("strictQuery", false);
mongoose.connect(mongoDB, { useNewUrlParser: true })

app.use(function (req, res, next) {
  res.header('Content-Type', 'application/json;charset=UTF-8')
  res.header('Access-Control-Allow-Credentials', true)
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  )
  next()
})

app.use(cors(
  {
    origin: ["http://localhost:3000", "http://45.33.72.121:3000"],
    exposedHeaders: ['X-Total-Count', 'Access-Control-Allow-Headers']
  }
))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.urlencoded({
  extended:true, 
}))

app.use(cookieParser());

app.use('/api/upload',verifyToken, UploadRouter)
app.use('/api/auth', Authrouter)
app.use('/api/semillas', Semillarouter)
app.use('/api/arboles', Arbolrouter)
app.use('/api/reservas',  Reservarouter)
app.use('/api/users', Userrouter)
app.use('/api/paises', Paisrouter)
app.use('/api/cursos', CoursesRouter)
app.use('/api/checkout',verifyToken, CheckoutRouter)


app.listen(process.env.API_PORT, () => console.log('AdminJS is under localhost:' + process.env.API_PORT.toString() + '/admin'))