import User from '../models/user.js';

async function createUser(email, given_name, family_name, picture, provider, password, country) {
    const userData = {
        email: email,
        name: given_name,
        lastname: family_name,
        roles: { User: 7080 },
        photo: picture,
        provider: provider
    };

    if (provider === 'local') {
        userData.password = password;
        userData.country = country;
    }

    const user = new User(userData);
    await user.save();
    return user;
}

export { createUser };
