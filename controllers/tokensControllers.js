import jwt from 'jsonwebtoken'
const access_secret_key = process.env.ACCESS_TOKEN_KEY
const refresh_secret_key = process.env.REFRESH_TOKEN_KEY

async function createAccessToken(user) {
    //Access Token
    const accessToken = jwt.sign({ _id: user._id}, access_secret_key, { expiresIn: "20m" });
    return accessToken
}


async function createRefreshToken(user) {
    //RefreshToken
    const refreshToken = jwt.sign({ _id: user._id }, refresh_secret_key, { expiresIn: "1d" });
    return refreshToken;
}

export { createAccessToken, createRefreshToken }
