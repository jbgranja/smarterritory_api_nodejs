import mongoose from 'mongoose';

const { Schema } = mongoose;

const semillaSchema = new Schema({
    name: {
        required: true,
        type: String
    },
    reserva: { type: Schema.Types.ObjectId, ref: 'Reserva' },
    sciName: {
        required: true,
        type: String,

    },
    elevacion: {
        required: true,
        type: String
    },
    region: {
        required: true,
        type: String
    },
    description: {
        required: true,
        type: String
    },
    photoUrl: {
        required: false,
        type: String
    },
    videoUrl: {
        required: false,
        type: String
    },
    avaliable: {
        required: true,
        type: Boolean
    },

    category: {
        required: false,
        type: String
    },
    price:{
        required: true,
        type: Number
    },
    user: [{ type: Schema.Types.ObjectId, ref: 'User' }],
})

export default mongoose.model('Semilla', semillaSchema)
