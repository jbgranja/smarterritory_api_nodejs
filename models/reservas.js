import mongoose from 'mongoose';
const { Schema } = mongoose;


const reservaSchema = new Schema({
    _id: Schema.Types.ObjectId,
    description: {
        required: true,
        type: String
    },
    name: {
        required: true,
        type: String
    },
    semillas: [{ type: Schema.Types.ObjectId, ref: 'Semilla' }],
    city: {
        required: true,
        type: String
    },
    province: {
        required: true,
        type: String
    },
    coordinates: {
        type: String
    },
    photo: {
        required: false,
        type: String
    },
    maxFill: {
        required: true,
        type: Number
    },
    priceSeed: {
        required: true,
        type: Number
    }
})

export default mongoose.model('Reserva', reservaSchema)
