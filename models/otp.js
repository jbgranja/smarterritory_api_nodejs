import mongoose from 'mongoose';
const { Schema } = mongoose;

const otpSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    otp: {
        required: true,
        type: String
    },
    createdAt: {
        type: Date,
        default: Date.now,
        index: { expires: 300 }
    }
})
export default mongoose.model('OTP', otpSchema)
