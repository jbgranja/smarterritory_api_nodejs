import mongoose from 'mongoose';

const { Schema } = mongoose;


const userSchema = new Schema({
    email: {
        required: true,
        type: String
    },
    password: {
        required: true,
        type: Number
    },
    lastname: {
        required: true,
        type: String
    },
    photo: {
        required: true,
        type: Number
    }
})

export default mongoose.model('Admin', userSchema)
