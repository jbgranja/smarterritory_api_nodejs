import mongoose from 'mongoose';

const { Schema } = mongoose;


const treeSchema = new Schema({
    name: {
        required: true,
        type: String
    },
    description: {
        required: true,
        type: String
    },
    sciname: {
        required: true,
        type: String
    },
    location: {
        required: true,
        type: String
    },
    edad: {
        required: true,
        type: String
    },
    photo: {
        required: false,
        type: String
    },
    video: {
        required: false,
        type: String
    },
    avaliable: {
        required: true,
        type: Boolean
    },
    
    price: {
        required: true,
        type: String
    },
    user: [{ type: Schema.Types.ObjectId, ref: 'User' }],
})

export default mongoose.model('Arbol', treeSchema)
