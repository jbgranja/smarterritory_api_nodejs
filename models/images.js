import mongoose from 'mongoose';
const { Schema } = mongoose;

const imageSchema = new Schema({
    name: {
        required: true,
        type: String
    },
    url: {
        required: true,
        type: String
    },
    type: {
        required: true,
        type: String
    }
})

export default mongoose.model('Images', imageSchema)
