import mongoose from 'mongoose';

const { Schema } = mongoose;

const countrySchema = new Schema({
    name: {
        required: true,
        type: String
    },
    currency: {
        required: true,
        type: String
    },
    active: {
        required: true,
        type: Boolean
    }  
    
})


export default mongoose.model('Courses', countrySchema)
