import mongoose from 'mongoose';

const { Schema } = mongoose;

const courseSchema = new Schema({
    name: {
        required: true,
        type: String
    },
    description: {
        required: true,
        type: String
    },
    photo: {
        required: false,
        type: String
    },
    user: [{ type: Schema.Types.ObjectId, ref: 'User' }],
})


export default mongoose.model('Courses', courseSchema)
