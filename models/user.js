import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

const { Schema } = mongoose;
const userSchema = new Schema({
    email: {
        required: true,
        type: String,
        unique: true
    },
    name: {
        required: true,
        type: String
    },
    password: {
        type: String,
        default: null // Set the default value to an empty string
    },
    lastname: {
        type: String,
        default:null
    },
    country: {
        type: String,
        default: null // Set the default value to an empty string
    },
    photo: {
        type: String
    },
    refreshToken: {
        type: String
    },
    roles: {
        type: Object
    },
    provider:{
        type:String,
        enum:['local','google','facebook'],
        default:'local'
    },
    courses: [{ type: Schema.Types.ObjectId, ref: 'Courses' }],

}, {
    timestamps: true
})

userSchema.pre('save', function (next) {
    var user = this;

    if (!user.isModified('password')) {
        return next();
    }

    bcrypt.genSalt(10, function (err, salt) {
        if (err) return next(err);
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) return next(err);
            user.password = hash;
            next(); // Proceed to the next middleware or save operation
        })
    })

    
})

export default mongoose.model('User', userSchema)
