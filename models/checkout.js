import mongoose from 'mongoose';
const { Schema } = mongoose;
const checkoutSchema = new Schema({
    semillas: [{ type: Schema.Types.ObjectId, ref: 'Semilla' }],
    arboles: [{ type: Schema.Types.ObjectId, ref: 'Arbol' }],
    cursos: [{ type: Schema.Types.ObjectId, ref: 'Course' }],
    user: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    payment: {
    }
})

export default mongoose.model('Checkout', checkoutSchema)
