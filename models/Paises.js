import mongoose from 'mongoose';
const { Schema } = mongoose;
const countrySchema = new Schema({
    name: {
        required: true,
        type: String
    },
    flag: {
        required: false,
        type: String
    },
    code: {
        required: true,
        type: String
    }
});


export default mongoose.model('Country', courseSchema)
