const ROLES  = {
    "Admin": 9001,
    "User": 7080,
    "Support": 3001
}

export default ROLES;