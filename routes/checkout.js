import express from "express"
import Arbol from '../models/arbol.js'
import Semilla from "../models/semillas.js"
import jwt from 'jsonwebtoken'
import { sendMailer } from "../email/email.js"
const access_secret_key = process.env.ACCESS_TOKEN_KEY

const CheckoutRouter = express.Router()
CheckoutRouter.get('/', async (req, res) => {
    try {
        const arboles = await Arbol.find()
        res.json(arboles)
    }
    catch (err) {
        res.send('Error' + err)
    }
})

CheckoutRouter.post('/', async (req, res) => {
    const { category, reserva, quantity } = req.body;
    const authHeader = req.headers.authorization || req.headers.Authorization;

    if (!authHeader?.startsWith('Bearer ')) {
        return res.status(401).send("no token bearer");
    }

    const token = authHeader.split(' ')[1];
    jwt.verify(token, access_secret_key
        , (err, decoded) => {
            if (err) return res.status(403).send(err)
            req.user = decoded;
            if (reserva && category && quantity) {
                Semilla.find({ reserva: reserva, category: category, avaliable: true }).limit(quantity).exec(function (err, seeds) {
                    if (err) {
                        res.status(500).send(err);
                    }
                    seeds.map(function (seed) {
                        seed.user = [decoded._id];
                        seed.avaliable = false;
                        seed.save();

                        let options = {
                            from: 'examplemail@mail.com',
                            to: 'ggjuanb@hotmail.com',
                            subject: 'Invoices due',
                            html: '<p>dsdsds<p>'
                          };
                          

                        sendMailer(options);

                        console.log("semilla comprada");
                    })
                    res.status(200).json(seeds);
                });
            }
            else {
                res.status(501).send("Error missing body ")
            }
        });
})

CheckoutRouter.patch('/', (req, res) => {
    res.send('Hello World!')
})

export default CheckoutRouter