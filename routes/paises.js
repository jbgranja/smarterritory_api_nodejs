import express from "express"
import Arbol from '../models/arbol.js'


const Paisrouter = express.Router()

Paisrouter.get('/', async (req, res) => {
    try {
        const paises = await Pais.find()
        res.json(arboles)
    }
    catch (err) {
        res.send('Error' + err)
    }
})


Paisrouter.get('/:paisId', async (req, res) => {
    try {
        let arbol = await Arbol.findById(req.params.arbolId)
        res.json(arbol)
    }
    catch (err) {
        res.send('Error' + err)
    }
})



Paisrouter.delete('/:paisId', async (req, res) => {
    try {

        let arbol = await Arbol.remove({ _id: req.params.arbolId})
        res.json(curso.deletedCount)
    }
    catch (err) {
        res.send('Error' + err)
    }
})

Paisrouter.post('/', async (req, res) => {
    const { name, description, sciname, location, edad, photo, video } = req.body

    const arbol = new Arbol({
        name: name,
        description: description,
        sciname: sciname,
        location: location,
        edad: edad,
        photo: photo,
        video: video
    })

    arbol.save();
    res.json(arbol)
})



Paisrouter.patch('/', (req, res) => {
    res.send('Hello World!')
})


export default Paisrouter