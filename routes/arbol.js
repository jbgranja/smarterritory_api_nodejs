import express from "express"
import Arbol from '../models/arbol.js'
import { verifyToken } from "../middleware/auth.js"
import ROLES from "../config/roles.js"
import verifyRoles from "../middleware/verifyRole.js"

const Arbolrouter = express.Router()

Arbolrouter.get('/', async (req, res) => {
    let user = req.query.user;
    if (user) {
        let res_query = await Arbol.find({ 'user': user }).exec();
        if (!res_query) {
            res.status(404).send("not found")
        }
        res.json(res_query)
    }
    else {
        try {
            const arboles = await Arbol.find()
            res.json(arboles)
        }
        catch (err) {
            res.send('Error' + err)
        }
    }
})
Arbolrouter.get('/:arbolId', async (req, res) => {
    try {
        let arbol = await Arbol.findById(req.params.arbolId)
        res.json(arbol)
    }
    catch (err) {
        res.send('Error' + err)
    }
})



Arbolrouter.delete('/:arbolId', verifyToken, async (req, res) => {
    try {
        let arbol = await Arbol.remove({ _id: req.params.arbolId })
        res.json(curso.deletedCount)
    }
    catch (err) {
        res.send('Error' + err)
    }
});

Arbolrouter.post('/', [verifyToken, verifyRoles(ROLES.Admin)], async (req, res) => {
    const { name, description, price, sciname, location, edad, photo, video } = req.body
    const arbol = new Arbol({
        name: name,
        description: description,
        sciname: sciname,
        location: location,
        edad: edad,
        photo: photo,
        video: video,
        price: price,
        avaliable: true
    })
    arbol.save();
    res.json(arbol);
})

Arbolrouter.patch('/:arbolId', [verifyToken, verifyRoles(ROLES.User)], async (req, res) => {
    try {
        let updateduser = { user: [req.user._id], avaliable: false };
        Arbol.findOneAndUpdate({ _id: req.params.arbolId, avaliable: true }, updateduser)
            .then(function (arbol) {
                if (arbol == null) {
                    console.log("arbol ya esta comprado");
                    res.status(409).json(arbol);
                }
                res.json(arbol);
            }).catch(function (err) {
                console.log(err);
            });
    }
    catch (err) {
        console.log(err);
        res.send('Error' + err)
    }
})

export default Arbolrouter