import express from "express"
import User from '../models/user.js'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import { OAuth2Client } from 'google-auth-library'
import axios from "axios"
import { createRefreshToken, createAccessToken } from '../controllers/tokensControllers.js';
import { createUser } from '../controllers/userController.js';

const access_secret_key = process.env.ACCESS_TOKEN_KEY
const refresh_secret_key = process.env.REFRESH_TOKEN_KEY
const Authrouter = express.Router()
const oAuth2Client = new OAuth2Client(
    "843222487359-863n5oi4k344e8p2718cnc5q9l4ek8gc.apps.googleusercontent.com",
    "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",
    'postmessage',
);
Authrouter.post('/verify-email-reset', async (req, res) => {
    const { email } = req.body
    console.log(email);

    const foundUser = await User.findOne({ email: email });

    if (!foundUser) {
        return
    }


    //Create Temporary OTP CODE

    //Send Mail to the requested Email
    res.status(200).send("Verified Email")

    // try {
    //     const user = jwt.verify(token, secret_key)
    //     const _id = user._id
    //     const hashed_password = bcrypt.hash(password, 10)

    //     User.updateOne(
    //         { _id },
    //         {
    //             $set: { hashed_password }
    //         }
    //     )
    //     res.json({ status: 'ok' })
    // }
    // catch (err) {

    //     console.log(err);
    // }
})

Authrouter.post('/reset-password', (req, res) => {
    const { password, otp } = req.body
    console.log(email);
    // try {
    //     const user = jwt.verify(token, secret_key)
    //     const _id = user._id
    //     const hashed_password = bcrypt.hash(password, 10)

    //     User.updateOne(
    //         { _id },
    //         {
    //             $set: { hashed_password }
    //         }
    //     )
    //     res.json({ status: 'ok' })
    // }
    // catch (err) {

    //     console.log(err);
    // }
})


Authrouter.get('/logout', async (req, res) => {
    const cookies = req.cookies;
    if (!cookies?.jwt) return res.sendStatus(204); //No content
    const refreshToken = cookies.jwt;
    // Is refreshToken in db?
    const foundUser = await User.findOne({ refreshToken })
    if (!foundUser) {
        res.clearCookie('jwt', { httpOnly: true, sameSite: 'None', secure: true });
        return res.sendStatus(204);
    }
    // Delete refreshToken in db
    foundUser.refreshToken = '';
    await foundUser.save();
    res.clearCookie('jwt', { httpOnly: true, sameSite: 'None', secure: true });
    res.sendStatus(204);
})

Authrouter.get('/refresh', async (req, res) => {
    const cookies = req.cookies;
    if (!cookies?.jwt) return res.sendStatus(401);

    const token = cookies.jwt;
    const foundUser = await User.findOne({ refreshToken: token });

    if (!foundUser) return res.sendStatus(403); //Forbidden 
    // evaluate jwt 
    jwt.verify(
        token, refresh_secret_key,
        (err, decoded) => {
            if (err || foundUser._id.toString() !== decoded._id) return res.status(403).send("not user id");
            const accessToken = createAccessToken(foundUser);
            res.json({ accessToken })
        }
    );
})

Authrouter.post('/admin/login', async (req, res) => {
    const { email, password } = req.body
    const user = await User.findOne({ email })

    if (!user) {
        return res.status(401).json({ status: 'error', error: 'Invalid username/password' })
    }

    if (await bcrypt.compare(password, user.password)) {

        const rolesArray = ['9001'];

        const result = roles.map(role => rolesArray.includes(role)).find(val => val === true);
        if (!result) {
            return res.status(401);
        }

        user.save()
        res.status(200).cookie("jwt", refreshToken, {
            httpOnly: true,
            maxAge: 24 * 60 * 60 * 1000
        })

        let user_id = user._id;
        res.json({ accessToken, user_id });
    }
    else {
        res.json({ status: 'error', error: 'Invalid username/password' })
    }
})

Authrouter.post('/google', async (req, res) => {
    const { tokens } = await oAuth2Client.getToken(req.body.code); // exchange code for tokens
    const user_info = jwt.decode(tokens.id_token);

    if (!user_info.email_verified) {
        res.status(204).send("Email not verified.");
        return;
    }

    const { email, given_name, family_name, picture } = user_info;
    const provider = "google";

    try {
        let user = await User.findOne({ email });

        if (!user) {
            user = await createUser(email, given_name, family_name, picture, provider);
            // Create a new user if the email doesn't exist
        }
        // Generate access and refresh tokens
        const accessToken = createAccessToken(user);
        const refreshToken = await createRefreshToken(user);

        user.refreshToken = refreshToken;
        await user.save();

        res.cookie("jwt", refreshToken, {
            httpOnly: true,
            maxAge: 24 * 60 * 60 * 1000,
        });
        res.json({ accessToken, user_id: user._id });
    } catch (error) {

        res.status(500).json({ message: 'Internal Server Error' });
    }
});


Authrouter.post('/facebook', async (req, res) => {
    const access_token = req.body.token; // exchange code for tokens
    const provider = "facebook"
    try {
        const response = await axios.get('https://graph.facebook.com/me?fields=email,id,first_name,last_name,picture&access_token=' + access_token);

        // Process the response data
        const user_info = response.data

        let email = user_info.email;
        let first_name = user_info.first_name;
        let last_name = user_info.last_name
        let picture = user_info.picture.data.url;

        //Check if is a new user to create account
        let user = await User.findOne({ email });

        if (!user) {
            user = await createUser(email, first_name, last_name, picture, provider);
        }

        const accessToken = createAccessToken(user);
        const refreshToken = await createRefreshToken(user);
        user.refreshToken = refreshToken

        res.cookie("jwt", refreshToken, {
            httpOnly: true,
            maxAge: 24 * 60 * 60 * 1000
        });
        try {
            user.save();
            let user_id = user._id;
            res.json({ accessToken, user_id });
        }
        catch (err) {
            console.log(err)
        }
    } catch (error) {
        // Handle the error
        console.error(error);
    }
});

Authrouter.post('/login', async (req, res) => {
    const { email, password } = req.body
    const user = await User.findOne({ email })

    if (!user) {
        return res.status(401).json({ status: 'error', error: 'Invalid Test' })
    }
    if (user.provider != "local") {
        return res.status(401).json({ status: 'bad login', error: "Invalid login provider" })
    }


    if (!await bcrypt.compare(password, user.password)) {
        // the username, password combination is successful
        const accessToken = createAccessToken(user);
        const refreshToken = await createRefreshToken(user);
        user.refreshToken = refreshToken
        user.save();

        res.cookie("jwt", refreshToken, {
            httpOnly: true,
            maxAge: 24 * 60 * 60 * 1000
        })
        res.json({ accessToken, user_id });
    }
    else {
        res.json({ status: 'error', error: 'Invalid username/password' })
    }
})

Authrouter.post('/signup', async (req, res) => {
    const { email, country, name, password, lastname } = req.body;
    const photo = "http://localhost:3080/public/data/uploads/profile/avatar.jpg";

    let user = await User.findOne({ email });
    if (user) return res.status(409).json("User already registered.");

    if (!user) {
        user = await createUser(email, first_name, last_name, picture, provider);

        try {
            await user.save()
            res.status(200).send("accout Created");
        }
        catch (err) {
            res.status(500).json("Something went wrong: " + err);
        }
    }

})

export default Authrouter