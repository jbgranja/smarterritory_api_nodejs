import express from "express"
import Images from '../models/images.js'
import multer from "multer"
import * as fs from 'fs'
import User from '../models/user.js'
import { verifyToken } from "../middleware/auth.js"

const UploadRouter = express.Router()

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        const obj = JSON.parse(JSON.stringify(req.body))
        const str = obj["directoryType"];
        const arr = ['semilla', 'profile', 'reserva'];

        const contains = arr.some(element => {
            if (str.includes(element)) {
                return true;
            }
        });

        if (!contains) {
            return res.status(401);
        }

        const dest = './public/data/uploads/' + obj["directoryType"];
        fs.access(dest, function (error) {
            if (error) {
                console.log("Directory does not exist.");
                return fs.mkdir(dest, (error) => cb(error, dest));
            } else {
                console.log("Directory exists.");
                return cb(null, dest);
            }
        });
    },
    filename: function (req, file, cb) {
        let imageExtension = file.mimetype.split("/")[1]
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
        cb(null, file.fieldname + '-' + uniqueSuffix + "." + imageExtension);
    }
})

const upload_profile = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
            //res.send("Only .png, .jpg and .jpeg format allowed!")

        }
    }
})

UploadRouter.post('/image', upload_profile.single('app_image'), function (req, res) {
    // req.file is the `avatar` file
    // req.body will hold the text fields, if there were any
    const image = new Images({
        name: req.file.filename,
        url: req.file.path,
        type: req.file.mimetype,
    })
    image.save();
    res.json(image);
})

UploadRouter.post('/image/profile', upload_profile.single('app_image'), function (req, res) {
    console.log(req.user._id);
    const image = new Images({
        name: req.file.filename,
        url: req.file.path,
        type: req.file.mimetype,
    })
    image.save();
    //Updating User with the new image url
    User.findByIdAndUpdate({ _id: req.user._id }, { photo: image.url }).then(function (user) {
        res.send(user);
    }).catch(function (err) {
        console.log(err);
        res.send(err);
    })
})

UploadRouter.delete('/image', upload_profile.single('app_image'), function (req, res) {
    // req.file is the `avatar` file
    // req.body will hold the text fields, if there were any
    res.send("ok image uploaded");
})

UploadRouter.patch('/image', upload_profile.single('app_image'), function (req, res) {
    // req.file is the `avatar` file
    // req.body will hold the text fields, if there were any
    res.send("ok image uploaded");

})


export default UploadRouter