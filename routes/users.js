import express from "express"
import User from '../models/user.js'
import { verifyToken } from "../middleware/auth.js"
const Userrouter = express.Router()
import jwt from 'jsonwebtoken'
const access_secret_key = process.env.ACCESS_TOKEN_KEY

Userrouter.get('/', async (req, res) => {
    try {
        const usuarios = await User.find()
        res.json(usuarios)
    }
    catch (err) {
        res.send('Error' + err)
    }
})

Userrouter.get('/:userId', async (req, res) => {
    try {
        let user = await User.findById(req.params.userId)
        res.json(user);
    }
    catch (err) {
        res.send('Error' + err)
    }
})

Userrouter.patch('/:userId', verifyToken, async (req, res) => {
    const { type } = req.body;
    const authHeader = req.headers.authorization || req.headers.Authorization;
    if (!authHeader?.startsWith('Bearer ')) {
        return res.status(401).send("no token bearer");
    }

    if (type) {
        const token = authHeader.split(' ')[1];
        // update for a course 
        jwt.verify(token, access_secret_key
            , (err, decoded) => {
                if (err) return res.status(403).send(err)
                try {
                    let user = User.findOneAndUpdate(req.user, { courses: decoded.id });
                    res.json(user);
                }
                catch (err) {
                    res.send('Error' + err)
                }
            });
    }
    else {
        return res.status(401).send("no type selected");
    }
})

Userrouter.post('/', async (req, res) => {
    const { email, name, password, lastname, country } = req.body
    const user = new User({
        name: name,
        email: email,
        password: password,
        lastname: lastname,
        country: country,
        roles: [{ "User": 7080 }, { "Admin": 9001 }]
    })

    user.save((err, doc) => {
        if (!err)
            res.send('User added successfully!' + user);
        else
            console.log('Error during record insertion : ' + err);
    });
})

Userrouter.delete('/:userId', verifyToken, async (req, res) => {
    try {
        let user = await user.remove({ id: req.params.userId })
        res.json(user.deletedCount)
    }
    catch (err) {
        res.send('Error' + err)
    }
})
export default Userrouter