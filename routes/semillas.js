import express from "express";
import Semilla from '../models/semillas.js';
import Reserva from '../models/reservas.js';

import { verifyToken} from "../middleware/auth.js"

const Semillarouter = express.Router()


Semillarouter.get('/', async (req, res) => {

    let category = req.query.category
    let reserva = req.query.reserva
    let avaliability = req.query.avaliable
    let user = req.query.user

    if (reserva && category && avaliability) {
        let res_query = await Semilla.find({ reserva, category, 'avaliable': avaliability }).exec();
        if (!res_query) {
            res.status(404).send("not found")
        }
        res.json(res_query)
    }

    else if (user) {
        console.log(user)
        let res_query = await Semilla.find({ 'user':user }).exec();
        if (!res_query) {
            res.status(404).send("not found")
        }
        res.json(res_query)
    }


    else if (reserva && avaliability) {
        let res_query = await Semilla.find({ reserva, 'avaliable':avaliability }).exec();
        if (!res_query) {
            res.status(404).send("not found")
        }
        res.json(res_query)
    }


    else if (reserva && category) {
        let res_query = await Semilla.find({ reserva, category }).exec();
        if (!res_query) {
            res.status(404).send("not found")
        }
        res.json(res_query)
    }


    else if (reserva) {
        let res_query = await Semilla.find({ reserva }).exec();
        if (!res_query) {
            res.status(404).send("not found")
        }
        res.json(res_query)
    }

    else {
        console.log('no query')
        try {
            const semillas = await Semilla.find()
            Semilla.count({}, function (err, count) {
                console.log("Number of semillas:", count);
                res.set('X-Total-Count', count).json(semillas);
            })
        }
        catch (err) {
            res.send('Error' + err)
        }
    }
})

Semillarouter.post('/', verifyToken, async (req, res) => {

    const { name, reservaId, sciName, description, photoUrl, videoUrl, avaliable, category, region, elevacion } = req.body
    //console.log(reservaId)

    const reserva = await Reserva.findById(reservaId);

    const newSemilla = new Semilla({
        name: name,
        reserva: reserva._id,
        sciName: sciName,
        description: description,
        photoUrl: photoUrl,
        videoUrl: videoUrl,
        avaliable: avaliable,
        category: category,
        region: region,
        elevacion: elevacion
    })
    try {
        const saveSeed = await newSemilla.save();
        reserva.semillas = reserva.semillas.concat(saveSeed._id);
        res.json(saveSeed)
    }
    catch (err) {
        res.send('Error' + err)
    }
})



Semillarouter.patch('/:semillaId',verifyToken, async (req, res) => {
    try {
        let semilla = await Semilla.findById(req.params.semillaId)
        res.json(semilla)
    }
    catch (err) {
        res.send('Error' + err)
    }
})




Semillarouter.get('/:semillaId', async (req, res) => {
    try {
        let semilla = await Semilla.findById(req.params.semillaId)
        res.json(semilla)
    }
    catch (err) {
        res.send('Error' + err)
    }
})


Semillarouter.delete('/:semillaId', verifyToken, async (req, res) => {
    try {
        let semilla = await Semilla.deleteOne({ _id: req.params.semillaId })
        res.json(semilla.deletedCount)
    }
    catch (err) {
        res.send('Error' + err)
    }
})


export default Semillarouter