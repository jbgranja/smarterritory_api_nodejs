import express from "express"
import Courses from '../models/courses.js'
import { verifyToken } from "../middleware/auth.js"

const CoursesRouter = express.Router()

CoursesRouter.get('/', async (req, res) => {

    let user = req.query.user
    if (user) {
        let res_query = await Courses.find({ 'user': user }).exec();
        if (!res_query) {
            res.status(404).send("not found")
        }
        res.json(res_query)
    }
    else {

        try {

            const cursos = await Courses.find()
            res.json(cursos)
        }
        catch (err) {
            res.send('Error' + err)
        }
    }

})

CoursesRouter.get('/:courseId', async (req, res) => {
    try {

        let curso = await Courses.findById(req.params.courseId)
        res.json(curso)
    }
    catch (err) {
        res.send('Error' + err)
    }
})


CoursesRouter.delete('/:courseId', verifyToken, async (req, res) => {
    try {
        let curso = await Courses.remove({ _id: req.params.courseId })
        res.json(curso.deletedCount)
    }
    catch (err) {
        res.send('Error' + err)
    }
})




CoursesRouter.patch('/:courseId', verifyToken, async (req, res) => {
    const { type } = req.body;

    const authHeader = req.headers.authorization || req.headers.Authorization;
    if (!authHeader?.startsWith('Bearer ')) {
        return res.status(401).send("no token bearer");
    }

    const token = authHeader.split(' ')[1];
    jwt.verify(token, access_secret_key
        , (err, decoded) => {
            if (err) return res.status(403).send(err)
            try {
                let updateduser = { user: [decoded._id] };
                Arbol.findOneAndUpdate({ _id: req.params.courseId}, updateduser, {
                    returnOriginal: false
                }).then(function (arbol) {
                    res.json(arbol);
                });
            }
            catch (err) {
                console.log(err);
                res.send('Error' + err)
            }
        });
})


CoursesRouter.post('/', verifyToken, async (req, res) => {
    const curso = new Courses({
        name: req.body.name,
        description: req.body.description,
        photo: req.body.photo,
    })

    try {
        const a1 = await curso.save();
        res.json(a1);
    }
    catch (err) {
        res.send('Error' + err)
    }
})




export default CoursesRouter