import express from "express"
import Reserva from '../models/reservas.js'
import Semilla from '../models/semillas.js'
import mongoose from "mongoose"
import { verifyToken } from "../middleware/auth.js"
import verifyRoles from "../middleware/verifyRole.js"
import ROLES from "../config/roles.js"

const Reservarouter = express.Router()

Reservarouter.get('/', async (req, res) => {
    try {

        const reservas = await Reserva.find().populate('semillas')
        res.json(reservas)
    }
    catch (err) {
        res.send('Error' + err)
    }
})

const verifyCredentials = [verifyToken, verifyRoles(ROLES.Admin)];
Reservarouter.post('/', verifyCredentials, (req, res) => {
    const { name, description, city, province, photo, coordinates, maxFill, priceSeed, categories } = req.body

    const reserva = new Reserva({
        _id: new mongoose.Types.ObjectId(),
        name: name,
        description: description,
        city: city,
        province: province,
        coordinates: coordinates,
        maxFill: maxFill,
        priceSeed: priceSeed,
        photo: photo
    })

    if (categories) {
        const reserva_id = reserva._id;
        categories.map(function (index) {
            console.log(index)
            for (let i = 0; i < parseInt(index.catmax); i++) {
                let newSemilla =
                    new Semilla({
                        _id: mongoose.Types.ObjectId(),
                        name: index.catname,
                        reserva: reserva_id,
                        sciName: index.catsci,
                        description: index.catdes,
                        avaliable: true,
                        region: index.catreg,
                        elevacion: index.catelev,
                        category: index.catname,
                        price: reserva.priceSeed
                    })
                newSemilla.save();
                reserva.semillas.push(newSemilla);
            }
        })
        reserva.save();

        res.json(reserva);
    }
})


Reservarouter.get('/:reservaId/:categories', async (req, res) => {
    try {
        const categories = await Semilla.find({ reserva: req.params.reservaId })
        let array_categories = [];
        categories.forEach(function (index) {
            if (array_categories.length <= 0) {
                array_categories.push(index.category)
            }
            else {
                if (!array_categories.find(element => element == index.category)) {
                    array_categories.push(index.category)
                }
            }
        })
        res.json({ categories: array_categories, maxSeeds: categories.length })
    }
    catch (err) {
        res.send('Error' + err)
    }
})


Reservarouter.delete('/:reservaId', verifyToken, async (req, res) => {
    try {
        let reserva = await user.remove({ id: req.params.reservaId })
        res.json(reserva.deletedCount)
    }
    catch (err) {
        res.send('Error' + err)
    }
})

Reservarouter.get('/:reservaId', async (req, res) => {
    try {
        let reserva = await Reserva.findById(req.params.reservaId).populate('semillas').exec()
        res.json(reserva)
    }
    catch (err) {
        res.send('Error' + err)
    }
})

export default Reservarouter