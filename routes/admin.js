import express from "express"
import User from '../models/user.js'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

const AdminRouter = express.Router()
const access_secret_key = process.env.ACCESS_TOKEN_KEY
const refresh_secret_key = process.env.REFRESH_TOKEN_KEY


AdminRouter.post('/login', async (req, res) => {

    const { email, password } = req.body
    const user = await User.findOne({ email })

    if (!user) {
        return res.status(401).json({ status: 'error', error: 'Invalid username/password' })
    }

    if (await bcrypt.compare(password, user.password)) {
        // the username, password combination is successful

        //Access Token
        const accessToken = jwt.sign({ email: user.email }, access_secret_key, { expiresIn: "10m" });

        //RefreshToken
        const refreshToken = jwt.sign({ _id: user._id, email: user.email }, refresh_secret_key, { expiresIn: "1d" });
        user.refreshToken = refreshToken
        user.save()
        res.status(200).cookie("jwt", refreshToken, {
            maxAge: 24 * 60 * 60 * 1000
        })
        let user_id = user._id;
        res.json({ accessToken, refreshToken, user_id });
    }

    else {
        res.json({ status: 'error', error: 'Invalid username/password' })
    }
})

AdminRouter.post('/signup', async (req, res) => {

    let { email } = req.body;
    let user = await User.findOne({ email });

    if (user) return res.status(409).json("User already registered.");

    user = new User({
        email: req.body.email,
        country: req.body.country,
        name: req.body.name,
        password: req.body.password,
        lastname: req.body.lastname,
    })

    try {
        const u1 = await user.save()
        res.json(u1)
    }
    catch (err) {
        res.status(500).json("Something went wrong: " + err);
    }
})

export default AdminRouter