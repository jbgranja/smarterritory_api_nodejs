import jwt from 'jsonwebtoken'
import 'dotenv/config'


const access_secret_key = process.env.ACCESS_TOKEN_KEY
const verifyToken = (req, res, next) => {

  const authHeader = req.headers.authorization || req.headers.Authorization;

  if (!authHeader?.startsWith('Bearer ')) return res.status(401).send("no token bearer");

  const token = authHeader.split(' ')[1];
  jwt.verify(token, access_secret_key
    , (err, decoded) => {
      if (err) return res.status(403).send(err)
      req.user = decoded;
      req.roles = decoded.roles
      next();
    });
};
export { verifyToken };