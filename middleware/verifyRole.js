
const verifyRoles = (...allowedRoles) => {

    return (req, res, next) => {
        if (!req?.roles) {
            res.status(401).send("no role requested");
        }
        const rolesArray = [...allowedRoles];
        const result = req.roles.map(role => rolesArray.includes(role)).find(val => val === true);
        if (!result) {
            return res.status(401);
        }
        console.log("role verified");
        next();
    }
}
export default verifyRoles;